from datetime import datetime

import requests
import xlsxwriter
from bs4 import BeautifulSoup

startTime1 = datetime.now()
cars = []
for i in range(1533383, 1534383):
    page = requests.get("https://turbo.az/autos/" + str(i))
    soup = BeautifulSoup(page.content, 'html.parser')
    ul = soup.find('ul', {'class': 'product-properties'})
    ps = soup.find('div', {'class': 'product-statistics'})
    if ul:
        cdate = ps.findChildren()[2]
        car = [i, cdate.text[11:]]
        lis = ul.findChildren('li', recursive=False)
        for li in lis:
            product_value = li.find('div', {'class': 'product-properties-value'})
            car.append(product_value.text)
        cars.append(car)
    if i in (1533399,1533683,1533883,1533993):
        print(i)
        print(datetime.now() - startTime1)
        print('-')
        workbook = xlsxwriter.Workbook('cars' + str(i) + '.xlsx')
        worksheet = workbook.add_worksheet()
        row = 0
        col = 0
        for row, data in enumerate(cars):
            worksheet.write_row(row, col, data)
        workbook.close()
workbook = xlsxwriter.Workbook('cars_final2.xlsx')
worksheet = workbook.add_worksheet()
row = 0
col = 0
for row, data in enumerate(cars):
    worksheet.write_row(row, col, data)
workbook.close()
